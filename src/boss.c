#include "geki3.h"
#include "extern.h"

Uint8 no;

/****************************
  ボス死亡
 ****************************/
RcHitEnum MoveBossEnd(CharacterData *my)
{
  my->X += ((rand() % 3) - 1) * 2;
  my->Y += 2;
  if (my->Y > AREA_RY)
    return RcHitBoss;
  Cchr.Attr     = AttrNone;
  Cchr.Target   = AttrNone;
  Cchr.X        = my->X + (rand() % my->Spr[0]->Image->Width * 0.8);
  Cchr.Y        = my->Y + (rand() % my->Spr[0]->Image->Height
			   * 0.8);
  Cchr.FrameMax = 13;
  Cchr.Spr      = PixBomb1;
  CopyYourNew(MoveBomb, HitNone, DrawPixmapFrame);
  KXL_PlaySound(SE_BOMB, KXL_SOUND_PLAY);
  return RcHitNone;
}

/****************************
  ボス死亡設定
 ****************************/
void CreateBossEnd(void)
{
  int sc[] = {1000,2000,3000,4000};

  Root->Your[BOSS_NO]->Chr.Attr     =
  Root->Your[BOSS_NO]->Chr.Target   = AttrNone;
  Root->Your[BOSS_NO]->Chr.Active   = True;
  Root->Your[BOSS_NO]->Chr.FrameNo  =
  Root->Your[BOSS_NO]->Chr.Cnt1     = 0;
  Root->Your[BOSS_NO]->Chr.Spr      = PixBoss;
  Root->Your[BOSS_NO]->Draw         = DrawPixmapFrame;
  Root->Your[BOSS_NO]->Hit          = HitNone;
  Root->Your[BOSS_NO]->Move         = MoveBossEnd;
  Root->Your[BOSS_NO]->Chr.Score    = sc[Root->Stage] + Root->StageLoop * 1000;
}

/****************************
  ボスの爆発
 ****************************/
RcHitEnum HitEnemyToLargeBomb(CharacterData *my, CharacterData *your)
{
  if (my->Cnt1 < 100)
    return RcHitNone;
  my->Hp -= your->Strong;
  if (my->Hp % 10 < 2) {
    Cchr.Attr     = AttrNone;
    Cchr.Target   = AttrNone;
    Cchr.X        = my->X + (rand() % my->Spr[0]->Image->Width * 0.8);
    Cchr.Y        = my->Y + (rand() % my->Spr[0]->Image->Height * 0.8);
    Cchr.FrameMax = 13;
    Cchr.Spr      = PixBomb1;
    CopyYourNew(MoveBomb, HitNone, DrawPixmapFrame);
    KXL_PlaySound(SE_BOMB, KXL_SOUND_PLAY);
  }
  if (my->Hp <= 0) {
    DeleteAll();
    CreateBossEnd();
  }
  return RcHitNone;
}

/****************************
  ボス設定
 ****************************/
void CreateBoss(void)
{
  RcHitEnum (*mv[])(CharacterData *my) = {
    MovePenguin, MoveTurtle, MoveIchthyosaur, MoveOctopus
  };
  int hp[] = {1000,1500,3000,5000};
  int mx[] = {3,6,4,4};

  no = 0;
  Root->Your[BOSS_NO]->Chr.Attr     = AttrEnemy;
  Root->Your[BOSS_NO]->Chr.Target   = AttrMy | AttrMShot;
  Root->Your[BOSS_NO]->Chr.Active   = True;
  Root->Your[BOSS_NO]->Chr.Strong   = 50;
  Root->Your[BOSS_NO]->Chr.FrameNo  =
  Root->Your[BOSS_NO]->Chr.Cnt1     =
  Root->Your[BOSS_NO]->Chr.Cnt2     =
  Root->Your[BOSS_NO]->Chr.Cnt3     =
  Root->Your[BOSS_NO]->Chr.Cnt4     =
  Root->Your[BOSS_NO]->Chr.Cnt5     =
  Root->Your[BOSS_NO]->Chr.Cnt6     =
  Root->Your[BOSS_NO]->Chr.Etc      = 0;
  Root->Your[BOSS_NO]->Chr.FrameMax = mx[Root->Stage];
  Root->Your[BOSS_NO]->Chr.Spr      = PixBoss;
  Root->Your[BOSS_NO]->Chr.X        = AREA_RX;
  Root->Your[BOSS_NO]->Chr.Y        = Root->Stage < 3 ? AREA_LY :
    AREA_RY - PixBoss[0]->Image->Height;
  Root->Your[BOSS_NO]->Chr.CntX     = Root->Your[BOSS_NO]->Chr.X << 8;
  Root->Your[BOSS_NO]->Chr.CntY     = Root->Your[BOSS_NO]->Chr.Y << 8;
  GetDirectionAdd(270, 
		  &(Root->Your[BOSS_NO]->Chr.AddX),
		  &(Root->Your[BOSS_NO]->Chr.AddY),
		  4);
  Root->Your[BOSS_NO]->Draw         = DrawPixmapFrame;
  Root->Your[BOSS_NO]->Hit          = HitEnemyToLargeBomb;
  Root->Your[BOSS_NO]->Move         = mv[Root->Stage];
  Root->Your[BOSS_NO]->Chr.Hp       = hp[Root->Stage] + Root->StageLoop * 100;
  Root->BossHpMax                   = Root->Your[BOSS_NO]->Chr.Hp;
  KXL_PlaySound(SE_WARNING, KXL_SOUND_STOP);
  KXL_PlaySound(SE_BOSS, KXL_SOUND_PLAY_LOOP);
}

/****************************
  ワーニング
 ****************************/
RcHitEnum MoveWarning(CharacterData *my)
{
  if (my->Cnt1 & 1)
    my->FrameNo ++;
  my->FrameNo &= 1;
  my->Cnt1 ++;
  if (my->Cnt1 == 125) {
    CreateBoss();
    return RcHitNone;
  } else if (my->Cnt1 > 125) {
    my->Y += 16;
    if (my->Y > AREA_RY)
      return RcHitDel;
  }
  return RcHitNone;
}

/****************************
  ワーニング
 ****************************/
void CreateWarning(void)
{
  Cchr.Attr   = AttrNone;
  Cchr.Target = AttrNone;
  Cchr.X      = AREA_LX + (DRAW_WIDTH -
    PixWarning[0]->Image->Width) / 2;
  Cchr.Y      = AREA_LY + (DRAW_HEIGHT -
    PixWarning[0]->Image->Height) / 2;
  Cchr.Spr    = PixWarning;
  CopyYourNew(MoveWarning, HitNone, DrawPixmapFrame);
  KXL_PlaySound(0, KXL_SOUND_STOP_ALL);
  KXL_PlaySound(SE_WARNING, KXL_SOUND_PLAY_LOOP);
}

/****************************
  ステージ1
 ****************************/
RcHitEnum MovePenguin(CharacterData *my)
{
  Sint16 i, s, ay;

  /*座標計算*/
  if (my->Cnt1 < 100) {
    my->X -= 2;
    my->Cnt1 ++;
    return RcHitNone;
  } else if (my->Cnt1 == 100) {
    my->Cnt1 = 101;
    my->Cnt3 = 4 + rand() % 4;
    my->Cnt4 = 0;
  }
  /*上下に移動*/
  my->Y += my->Cnt3;
  if (my->Y < AREA_LY || my->Y > AREA_RY - my->Spr[0]->Image->Height)
    my->Cnt3 = -(my->Cnt3);

  /*常時攻撃*/
  my->Cnt2 ++;
  if (my->Cnt2 % 20 == 0) {
    if (rand() % 3 == 0) {
      s = rand() % 20;
      for (i=200 + s; i < 320 + s; i += (10 + s))
	if (rand() % 2)
	  CreateEnemyShot(my->X + 20,
			   my->Y + 90,
			   i,
			   4, 0);
    }
    CreateEnemyShot(my->X + 20,
		     my->Y + 90,
		     GetDirection(my, &(Root->My[0]->Chr)),
		     6, 1);
  }
  /*マジ攻撃*/
  if (my->Cnt2 % 50 == 0) {
    my->Cnt4 = 1;
    my->Cnt5 = 0;
    my->Cnt3 = 0;
  }
  if (my->Cnt4) {
    if (my->Cnt5 < 4) { /*口を開く*/
      my->FrameNo += (my->Cnt5 & 1);
      my->Cnt5 ++;
    } else if (my->Cnt5 == 4) { /*攻撃*/
      if (++ my->Cnt4 < 19) {
	if (my->Etc > 800) {
	  s = (rand() % 20) - 10;
	  CreateEnemyBomb(my->X + 20,
			  my->Y + 54,
			  190 + my->Cnt4 * 10 + s,
			  15 + (rand() % 6));
	} else {
	  if (my->Cnt4 % 3 == 0) 
	    CreateEnemyBomb(my->X + 20,
			    my->Y + 54,
			    200 + my->Cnt4 * 10,
			    15 + (rand() % 6));
	}
      } else {
	my->Cnt5 ++;
      }
    } else { /*口を閉じる*/
      if (my->Cnt5 < 8) {
	my->FrameNo -= (my->Cnt5 & 1);
	my->Cnt5 ++;
      } else { /*移動準備*/
	my->Cnt4 = 0;
	my->Cnt3 = (rand() % 4) + (my->Y < AREA_LY + DRAW_HEIGHT / 2 ?
				   4 : -4);
      }
    }
  }
  if (my->Etc < 2000)
    my->Etc ++;
  return RcHitNone;
}

/****************************
  ステージ2
 ****************************/
RcHitEnum MoveTurtle(CharacterData *my)
{
  Uint8 anime[] = {0,0,1,1,2,2,1,1};
  Sint16 i, s, ay;

  /*座標計算*/
  if (my->Cnt1 < 100) {
    my->X -= 2;
    my->Cnt1 ++;
    return RcHitNone;
  } else if (my->Cnt1 == 100) {
    my->Cnt1 = 101;
    my->Cnt3 = 4 + rand() % 4;
    my->Cnt4 = 0;
    my->Cnt6 = -4;
  }
  /*泳ぐ*/
  if (my->Cnt4 == 0) {
    my->FrameNo = anime[no];
    if (++ no > 7)
      no = 0;
  }

  /*上下に移動*/
  my->Y += my->Cnt3;
  if (my->Y < AREA_LY ||
      my->Y > AREA_RY - my->Spr[0]->Image->Height)
    my->Cnt3 = -(my->Cnt3);
  /*左右に移動*/
  if (my->Cnt4 == 0) {
    my->X += my->Cnt6;
    if (my->X < AREA_LX + 200 ||
	my->X > AREA_RX - my->Spr[0]->Image->Width)
      my->Cnt6 = -(my->Cnt6);
  }
  /*常時攻撃*/
  my->Cnt2 ++;
  if (my->Cnt2 % 20 == 0) {
    if (rand() % 3 == 0) {
      s = rand() % 20;
      for (i=200 + s; i < 320 + s; i += (10 + s))
	CreateEnemyShot(my->X + 26,
			 my->Y + 60,
			 i,
			 4 + rand() % 4, 1);
    }
  }
  /*マジ攻撃*/
  if (my->Cnt2 % 80 == 0 && rand() % 2 == 0) {
    my->Cnt4 = 1;
    my->Cnt5 = 0;
    my->Cnt3 = 0;
    /*無敵*/
    my->FrameNo = 3;
    my->Attr   = AttrEShot;
    my->Target = AttrMy;
  }
  if (my->Cnt4) {
    if (my->Cnt5 < 4) { /*砲台出現*/
      my->FrameNo += (my->Cnt5 & 1);
      my->Cnt5 ++;
    } else if (my->Cnt5 == 4) { /*攻撃*/
      if (++ my->Cnt4 < 65) {
	if (my->Cnt4 % (my->Etc < 800 ? 16 : 8) == 0) 
	  CreateMissile(my, my->X - 8, my->Y);
      } else {
	my->Cnt5 ++;
      }
    } else { /*砲台隠れる*/
      if (my->Cnt5 < 8) {
	my->FrameNo -= (my->Cnt5 & 1);
	my->Cnt5 ++;
      } else { /*移動準備*/
	my->Cnt4 = 0;
	my->Cnt3 = (rand() % 4) + (my->Y < AREA_LY + DRAW_HEIGHT / 2 ?
				   4 : -4);
	/*無敵解除*/
	my->FrameNo = 0;
	my->Attr   = AttrEnemy;
	my->Target = AttrMy | AttrMShot;
      }
    }
  }
  if (my->Etc < 2000)
    my->Etc ++;
  return RcHitNone;
}

/****************************
  ステージ3
 ****************************/
RcHitEnum MoveIchthyosaur(CharacterData *my)
{
  Uint8 anime[] = {0,0,0,0,1,1,1,1,2,2,2,2,1,1,1,1};
  Sint16 i, s, ay;

  /*座標計算*/
  if (my->Cnt1 < 100) {
    my->X -= 3;
    my->Cnt1 ++;
    return RcHitNone;
  } else if (my->Cnt1 == 100) {
    my->Cnt1 = 101;
    my->Cnt3 = 4 + rand() % 4;
    my->Cnt4 = 0;
    my->Cnt6 = -2;
  }
  /*泳ぐ*/
  if (my->Cnt4 == 0) {
    my->FrameNo = anime[no];
    if (++ no > 15)
      no = 0;
  }

  /*上下に移動*/
  my->Y += my->Cnt3;
  if (my->Y < AREA_LY ||
      my->Y > AREA_RY - my->Spr[0]->Image->Height)
    my->Cnt3 = -(my->Cnt3);
  /*左右に移動*/
  if (my->Cnt4 == 0) {
    my->X += my->Cnt6;
    if (my->X < AREA_LX + 200 ||
	my->X > AREA_RX - my->Spr[0]->Image->Width + 50)
      my->Cnt6 = -(my->Cnt6);
  }
  /*常時攻撃*/
  my->Cnt2 ++;
  if (my->Cnt2 % 20 == 0) {
    if (rand() % 2 == 0) {
      CreateMissile(my, my->X + 124, my->Y + 22);
      CreateMissile(my, my->X + 124, my->Y + 84);
    }
    if (rand() % 3 == 0) {
      s = rand() % 20;
      for (i=200 + s; i < 320 + s; i += (10 + s))
	CreateEnemyShot(my->X + 60,
			 my->Y + 86,
			 i,
			 4 + rand() % 4, 1);
    }
  }
  /*マジ攻撃*/
  if (my->Cnt2 % 80 == 0 && rand() % 2 == 0) {
    my->Cnt4 = 1;
    my->Cnt3 = 0;
    my->FrameNo = 3;
    if (rand() % 2)
      CreateCuttleFish(my->X + 30, my->Y + 66,
		       (rand() % 60) + 240, 10,
		       rand() % 100 > 20 ? False : True);
  }
  if (my->Cnt4) {
    if (++ my->Cnt4 < 50) {
      if (my->Cnt4 % 4 == 0) {
	s = (rand() % 20) - 10;
	CreateEnemyBomb(my->X + 60,
			my->Y + 106,
			180 + my->Cnt4 * 8 + s,
			15 + (rand() % 6));
      }
    } else { /*移動準備*/
      my->Cnt4 = 0;
      my->Cnt3 = (rand() % 4) + (my->Y < AREA_LY + DRAW_HEIGHT / 2 ?
				 4 : -4);
    }
  }
  if (my->Etc < 2000)
    my->Etc ++;
  return RcHitNone;
}

/****************************
  ステージ４
 ****************************/
RcHitEnum MoveOctopus(CharacterData *my)
{
  Uint8 anime[] = {0,0,0,0,1,1,1,1,2,2,2,2,1,1,1,1};
  Sint16 i, s, ay;

  /*座標計算*/
  if (my->Cnt1 < 100) {
    my->X -= 3;
    my->Cnt1 ++;
    return RcHitNone;
  } else if (my->Cnt1 == 100) {
    my->Cnt1 = 101;
    my->Cnt4 = 0;
    my->Cnt6 = -2;
  }
  /*泳ぐ*/
  if (my->Cnt4 == 0) {
    my->FrameNo = anime[no];
    if (++ no > 15)
      no = 0;
  }

  /*左右に移動*/
  if (my->Cnt4 == 0) {
    my->X += my->Cnt6;
    if (my->X < AREA_LX + 200 ||
	my->X > AREA_RX - my->Spr[0]->Image->Width + 50)
      my->Cnt6 = -(my->Cnt6);
  }
  /*常時攻撃*/
  my->Cnt2 ++;
  if (my->Cnt2 % 20 == 0) {
    if (rand() % 2 == 0) {
      CreateMissile(my, my->X + 48, my->Y + 94);
    }
    if (rand() % 3 == 0) {
      s = rand() % 20;
      for (i=200 + s; i < 320 + s; i += (10 + s))
	CreateEnemyShot(my->X + 48,
			my->Y + 94,
			i,
			4 + rand() % 4, rand() % 2);
    }
  }
  /*マジ攻撃*/
  if (my->Cnt2 % 80 == 0 && rand() % 2 == 0) {
    my->Cnt4 = 1;
    my->FrameNo = 3;
    CreateHeadHammerShark(AREA_LY + 32, False);
    CreateSeeUrchin();
  }
  if (my->Cnt4) {
    if (++ my->Cnt4 < 50) {
      if (my->Cnt4 % 3 == 0) {
	s = (rand() % 10) - 10;
	CreateEnemyBomb(my->X + 20,
			my->Y + 88,
			120 + my->Cnt4 * 8 + s,
			15 + (rand() % 6));
      }
    } else { /*移動準備*/
      my->Cnt4 = 0;
    }
  }
  if (my->Etc < 2000)
    my->Etc ++;
  return RcHitNone;
}
