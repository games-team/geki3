#ifndef _BOSS_H_
#define _BOSS_H_

RcHitEnum HitEnemyToLargeBomb(CharacterData *my, CharacterData *your);
RcHitEnum MoveWarning(CharacterData *my);
void CreateWarning(void);
RcHitEnum MovePenguin(CharacterData *my);
RcHitEnum MoveTurtle(CharacterData *my);
RcHitEnum MoveIchthyosaur(CharacterData *my);
RcHitEnum MoveOctopus(CharacterData *my);
RcHitEnum MoveBossEnd(CharacterData *my);

#endif
