#ifndef _EXTERN_H_
#define _EXTERN_H_

#include "misc.h"
#include "opening.h"
#include "game.h"
#include "load.h"
#include "my.h"
#include "your.h"
#include "boss.h"
#include "ranking.h"

#ifdef _EXTERN_DEF_
#define EXTERN 
#else
#define EXTERN extern
#endif
EXTERN CharacterData Cchr;          /**                **/
EXTERN RootData *Root;              /** root data      **/
EXTERN RankingData **Ranking;       /** ranking data   **/
EXTERN StageData **StageDatas;      /** stage data     **/
EXTERN PixData **PixMy;             /** my(4)          **/
EXTERN PixData **PixMyShot;         /** my shot(2)     **/
EXTERN PixData **PixMyMissile;      /** my missile(2)  **/
EXTERN PixData **PixMyLaser;        /** my laser(1)    **/
EXTERN PixData **PixMyWide;         /** my wide(1)     **/
EXTERN PixData **PixBall;           /** ball(3)        **/
EXTERN PixData **PixBomb1;          /** bomb1(13)      **/
EXTERN PixData **PixBomb2;          /** bomb2(9)       **/
EXTERN PixData **PixTobi;           /** Tobi(5)        **/
EXTERN PixData **PixIka;            /** Ika(3)         **/
EXTERN PixData **PixIkaBall;        /** IkaBall(3)     **/
EXTERN PixData **PixUni;            /** Uni(2)         **/
EXTERN PixData **PixMogu;           /** Mogu(1)        **/
EXTERN PixData **PixMoguBall;       /** MoguBall(1)    **/
EXTERN PixData **PixPata;           /** Pata(3)        **/
EXTERN PixData **PixPataBall;       /** PataBall(3)    **/
EXTERN PixData **PixEi;             /** Ei(6)          **/
EXTERN PixData **PixEiBall;         /** EiBall(6)      **/
EXTERN PixData **PixKura;           /** kura(9)        **/
EXTERN PixData **PixManboo;         /** manboo(5)      **/
EXTERN PixData **PixEnemyShot1;     /** enemy shot1(1) **/
EXTERN PixData **PixEnemyShot2;     /** enemy shot2(6) **/
EXTERN PixData **PixMissile;        /** missile(12)    **/
EXTERN PixData **PixEBomb;          /** enemy bomb(3)  **/
EXTERN PixData **PixBoss;           /** boss **/
EXTERN PixData **PixWarning;        /** warning(2) **/
EXTERN KXL_Image *star_image;
EXTERN KXL_Image *back_image;
EXTERN KXL_Image *hp_image;
EXTERN KXL_Image *frame_image;
EXTERN KXL_Image *power_image;
EXTERN _star star[20];

#endif
