#include "geki3.h"
#include "extern.h"

/****************************
  自分データ削除
 ****************************/
int DeleteMyData(Uint16 m)
{
  if (Root->My[m]->Chr.Active == True) {
    Root->My[m]->Chr.Active = False;
    Root->MyNo --;
    return True;
  }
  return False;
}

/****************************
  敵データ削除
 ****************************/
int DeleteYourData(Uint16 y)
{
  if (Root->Your[y]->Chr.Active == True) {
    Root->Your[y]->Chr.Active = False;
    Root->YourNo --;
    return True;
  }
  return False;
}

/****************************
  自分データ作成
 ****************************/
Sint16 CopyMyNew(RcHitEnum (*act)(CharacterData *my),
              RcHitEnum (*hit)(CharacterData *my, CharacterData *your),
              void (*re)(CharacterData *my))
{
  Uint16 i;
  
  if (Root->MyNo >= MAX_MY)
    return -1;
  for (i = 1; i < MAX_MY; i ++) {
    if (Root->My[i]->Chr.Active == False) {
      Root->My[i]->Chr         = Cchr;
      Root->My[i]->Chr.CntX    = Root->My[i]->Chr.X << 8;
      Root->My[i]->Chr.CntY    = Root->My[i]->Chr.Y << 8;
      Root->My[i]->Move        = act;
      Root->My[i]->Hit         = hit;
      Root->My[i]->Draw        = re;
      Root->My[i]->Chr.Active  = True;
      Root->My[i]->Chr.FrameNo =
      Root->My[i]->Chr.Cnt1    =
      Root->My[i]->Chr.Cnt2    = 0;
      Root->MyNo ++;
      return i;
    }
  }
}

/****************************
 敵データ作成
 ****************************/
Sint16 CopyYourNew(RcHitEnum (*act)(CharacterData *my),
                RcHitEnum (*hit)(CharacterData *my, CharacterData *your),
                void (*re)(CharacterData *my))
{
  Uint16 i;
  Uint16 st = 41;
  Uint16 ed = 81;
  
  if (Root->YourNo >= MAX_YOUR)
    return -1;
  if (Cchr.Attr == AttrEnemy) {
    /** 地上の敵 **/
    st = 1;
    ed = st + 30;
  } else if (Cchr.Attr & (AttrEShot | AttrEBomb)) {
    /** 爆発 **/
    st = 81;
    ed = MAX_YOUR;
  }
  for (i = st; i < ed; i ++) {
    if (Root->Your[i]->Chr.Active == False) {
      Root->Your[i]->Chr         = Cchr;
      Root->Your[i]->Chr.CntX    = Root->Your[i]->Chr.X << 8;
      Root->Your[i]->Chr.CntY    = Root->Your[i]->Chr.Y << 8;
      Root->Your[i]->Move        = act;
      Root->Your[i]->Hit         = hit;
      Root->Your[i]->Draw        = re;
      Root->Your[i]->Chr.Active  = True;
      Root->Your[i]->Chr.FrameNo =
      Root->Your[i]->Chr.Cnt1    =
      Root->Your[i]->Chr.Cnt2    = 0;
      Root->YourNo ++;
      return i;
    }
  }
}

/****************************
  敵データ削除
 ****************************/
void DeleteAll(void)
{
  Uint16 i;

  for (i = 1; i < MAX_YOUR; i ++) {
    if (Root->Your[i]->Chr.Active == False)
      continue;
    Root->Your[i]->Chr.Attr    =
    Root->Your[i]->Chr.Target  = AttrNone;
    Root->Your[i]->Chr.Spr     = PixBomb1;
    Root->Your[i]->Chr.Cnt1    =
    Root->Your[i]->Chr.FrameNo = 0;
    Root->Your[i]->Move        = MoveBomb;
    Root->Your[i]->Hit         = HitNone;
    Root->Your[i]->Draw        = DrawPixmapFrame;
  }
}

/****************************
  当り判定
 ****************************/
Bool Check(CharacterData *my, CharacterData *your)
{
  PixData *m = my->Spr[my->FrameNo];
  PixData *y = your->Spr[your->FrameNo];
  KXL_Rect mm, yy;
  
  mm.Left   = my->X + m->r.Left;
  mm.Top    = my->Y + m->r.Top;
  mm.Width  = m->r.Width;
  mm.Height = m->r.Height;
  yy.Left   = your->X + y->r.Left;
  yy.Top    = your->Y + y->r.Top;
  yy.Width  = y->r.Width - 1;
  yy.Height = y->r.Height - 1;
  return KXL_RectIntersect(mm, yy);
}

/****************************
  ゲームメイン
 ****************************/
void Game(void)
{
  RcHitEnum rc;
  Uint16 i;
  Sint16 m, y;
  KXL_Rect r;

  r.Left = AREA_LX;
  r.Top = AREA_LY;
  r.Width = DRAW_WIDTH;
  r.Height = DRAW_HEIGHT;
  KXL_ClearFrame(r);
  /** 背景スクロール **/
  /** 前半描画 **/
  r.Left   = Root->ScrollCnt;
  r.Top    = 0;
  r.Width  = 550 - Root->ScrollCnt;
  r.Height = 200;
  KXL_PutRectImage(back_image, r,
		   AREA_LX, 
		   AREA_LY + (Root->Stage == 0 ? 0 : 200));
  /** 後半描画 **/
  r.Left = 0;
  r.Width = Root->ScrollCnt ? Root->ScrollCnt : 2;
  KXL_PutRectImage(back_image, r,
		   AREA_RX - Root->ScrollCnt,
		   AREA_LY + (Root->Stage == 0 ? 0 : 200));
  Root->ScrollCnt += 2;
  if (Root->ScrollCnt == 550)
    Root->ScrollCnt = 0;
  /* star move */
  for (i=0; i<20; i++) {
    star[i].x-=star[i].add;
    if (star[i].x<-4) {
      star[i].x=AREA_RX;
      star[i].y=AREA_LY + (rand()%DRAW_HEIGHT);
      star[i].add=8+(rand()%4);
    }
    KXL_PutImage(star_image, star[i].x, star[i].y);
  }
  
  /** 敵等発生 **/
  for (i = 0; i < Root->StageMax; i ++) {
    if (Root->EnemyCnt == StageDatas[i]->Time &&
        StageDatas[i]->Flag == False) {
      StageDatas[i]->Flag = True;
    }
  }
  for (i = 0; i < Root->StageMax; i ++) {
    if (StageDatas[i]->Flag == True) {
      if (StageDatas[i]->Max) {
	if (!StageDatas[i]->StepTime) {
	  StageDatas[i]->Max --;
	  StageDatas[i]->StepTime = StageDatas[i]->Step;
	  switch (StageDatas[i]->CreateNo) {
	  case 0: /* ワーニング出現 */
            CreateWarning();
            break;
	  case 1: /* いか */
            CreateCuttleFish(AREA_RX,
                             AREA_LY + rand() % (DRAW_HEIGHT - PixIka[0]->Image->Height),
                             260 + rand() % 20,
                             8 + Root->StageLoop,
                             False);
	    break;
	  case 2: /* うに */
	    CreateSeeUrchin();
	    break;
	  case 3: /* もぐ */
	    CreateSubmarine(False);
	    break;
	  case 4: /* トビ */
	    CreateFlyingFish(AREA_LY - PixTobi[0]->Image->Height, 0);
	    break;
	  case 5: /* トビ */
	    CreateFlyingFish(AREA_RY, 1);
	    break;
	  case 6: /* もぐ(ボール) */
	    CreateSubmarine(True);
	    break;
	  case 7: /* ぱた */
	    CreateHeadHammerShark(AREA_LY + 32, False);
	    break;
	  case 8: /* ぱた */
	    CreateHeadHammerShark(AREA_RY - PixPata[0]->Image->Height - 32, False);
	    break;
	  case 9: /* ぱた(ボール) */
	    CreateHeadHammerShark(AREA_LY - 16 +  DRAW_HEIGHT / 2, True);
	    break;
	  case 10: /* いか(ボール) */
	    CreateCuttleFish(AREA_RX,
                             AREA_LY + rand() % (DRAW_HEIGHT - PixIka[0]->Image->Height),
                             260 + rand() % 20,
                             8 + Root->StageLoop,
                             True);
	    break;
          case 11: /* まんぼう */
            CreateOceanSunFish();
            break;
          case 12: /* えい */
            CreateEi(False);
            break;
          case 13: /* えい(ボール) */
            CreateEi(True);
            break;
          case 14: /* くらげ */
	    for (y = 0; y < 7; y ++)
	      CreateJellyFish(AREA_LX + 100 + y * 60);
            break;
	  }
	} else
          StageDatas[i]->StepTime --;
      } else
        StageDatas[i]->Flag = False;
    }
  }
  if (Root->EnemyCnt < 10000)
    Root->EnemyCnt ++;
  /** 自分移動 **/
  for (m = 0; m < MAX_MY; m ++)
    if (Root->My[m]->Chr.Active == True)
      if ((rc = Root->My[m]->Move(&(Root->My[m]->Chr))) == RcHitDel)
        DeleteMyData(m);
  /** 敵移動 **/
  for (y = 0; y < MAX_YOUR; y ++) {
    if (Root->Your[y]->Chr.Active == False)
      continue;
    rc = Root->Your[y]->Move(&(Root->Your[y]->Chr));
    if (rc == RcHitDel) {
      DeleteYourData(y);
    } else if (rc == RcHitBoss) {
      Root->Cnt = 0;
      Root->MainFlag = MainClear;
      Root->Score += Root->Your[y]->Chr.Score;
      DeleteYourData(y);
      return;
    }
  }
  /** 当り判定 **/
  for (m = 0; m < MAX_MY; m ++) {
    if (Root->My[m]->Chr.Active == False)
      continue;
    for (y = 0; y < MAX_YOUR; y ++) {
      if (Root->Your[y]->Chr.Active == False)
        continue;
      if (Root->My[m]->Chr.Target & Root->Your[y]->Chr.Attr) {
        if (Check(&(Root->My[m]->Chr), &(Root->Your[y]->Chr)) == False)
          continue;
        if (Root->My[m]->Hit(&(Root->My[m]->Chr), &(Root->Your[y]->Chr)) == RcHitDel)
          DeleteMyData(m);
        rc = Root->Your[y]->Hit(&(Root->Your[y]->Chr), &(Root->My[m]->Chr));
        switch (rc) {
        case RcHitBomb:
          Root->Score += Root->Your[y]->Chr.Score;
        case RcHitDel:
          DeleteYourData(y);
          break;
        default:
          break;
        }
      }
    }
  }
  /** 相手描画 **/
  for (y = 0; y < MAX_YOUR; y ++)
   if (Root->Your[y]->Chr.Active == True)
     Root->Your[y]->Draw(&(Root->Your[y]->Chr));
  /** 自分描画 **/
  for (m = MAX_MY - 1; m >= 0; m --)
    if (Root->My[m]->Chr.Active == True)
      Root->My[m]->Draw(&(Root->My[m]->Chr));
  /*インフォメーション描画*/
  Infomation();
}
