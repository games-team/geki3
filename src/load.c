#include "geki3.h"
#include "extern.h"

void SetPer(PixData *my, float per)
{
  my->r.Width  = (Uint16)((float)my->Image->Width  * per + 0.5);
  my->r.Height = (Uint16)((float)my->Image->Height * per + 0.5);
  my->r.Left   = (my->Image->Width  - my->r.Width)  / 2;
  my->r.Top    = (my->Image->Height - my->r.Height) / 2;
}

/**********************
  ピックスマップロード
  分割
 **********************/
PixData **LoadPixmapSplit(Uint8 *fname, Uint8 blend, Uint16 max, float per)
{
  Uint8 filename[64];
  Uint16 w, h, i;
  KXL_Image *img;
  PixData **new;
  KXL_Rect rect;

  /* xpmファイル読み込み */
  sprintf(filename, BMP_PATH "/%s.bmp", fname);
  img = KXL_LoadBitmap(filename, blend);
  w = img->Width / max;
  h = img->Height;
  /* イメージの分割 */
  new = (PixData **)KXL_Malloc(sizeof(PixData *) * max);
  for (i = 0; i < max; i ++) {
    new[i] = (PixData *)KXL_Malloc(sizeof(PixData));
    rect.Left   = i * w;
    rect.Top    = 0;
    rect.Width  = w;
    rect.Height = h;
    new[i]->Image = KXL_CopyImage(img, rect);
    SetPer(new[i], per);
  }
  /* 読み込み用Imageを解放 */
  KXL_DeleteImage(img);
  return new;
}

/**********************
  ピックスマップロード
 **********************/
PixData *LoadPixmap(Uint8 *fname, Uint8 blend, float per)
{
  Uint8 filename[64];
  PixData *new;

  /* xpmファイル読み込み */
  sprintf(filename, BMP_PATH "/%s.bmp", fname);
  new = (PixData *)KXL_Malloc(sizeof(PixData));
  new->Image = KXL_LoadBitmap(filename, blend);
  SetPer(new, per);
  return new;
}

/**********************
  ピックスマップロード
 **********************/
PixData **LoadPixmaps(Uint8 *fname, Uint8 blend, Uint16 max, float per)
{
  PixData **new;
  Uint8 filename[64];
  Uint16 i;

  new = (PixData **)KXL_Malloc(sizeof(PixData *) * max);
  for (i = 0; i < max; i ++) {
    sprintf(filename, "%s%d", fname, i + 1);
    new[i] = LoadPixmap(filename, blend, per);
  }
  return new;
}

/**********************
  ピックスマップ解放
 **********************/
void UnLoadPixmap(PixData *my)
{
  KXL_DeleteImage(my->Image);
}

/**********************
  ピックスマップ解放
 **********************/
void UnLoadPixmaps(PixData **my, Uint16 max)
{
  while (max)
    UnLoadPixmap(my[-- max]);
  KXL_Free(my);
}

/**********************
  ピックスマップ作成
 **********************/
void CreatePixmap(void)
{
  Uint16 i;

  PixMy         = LoadPixmapSplit("my", 0, 4, 0.4);
  PixMyShot     = LoadPixmapSplit("my_shot", 4, 2, CHECK_PER);
  PixMyMissile  = LoadPixmapSplit("my_missile", 0, 2, CHECK_PER);
  PixMyLaser    = LoadPixmapSplit("my_laser", 0, 1, CHECK_PER);
  PixMyWide     = LoadPixmapSplit("my_wide", 0, 1, CHECK_PER);
  PixBall       = LoadPixmapSplit("ball", 0, 3, 0);
  PixBomb1      = LoadPixmapSplit("bomb1", 0, 13, CHECK_PER);
  PixBomb2      = LoadPixmapSplit("bomb2", 0, 9, CHECK_PER);
  PixTobi       = LoadPixmapSplit("tobi", 0, 5, CHECK_PER);
  PixIka        = LoadPixmapSplit("ika", 0, 3, CHECK_PER);
  PixIkaBall    = LoadPixmapSplit("ika_ball", 0, 3, CHECK_PER);
  PixUni        = LoadPixmapSplit("uni", 0, 2, CHECK_PER);
  PixMogu       = LoadPixmapSplit("mogu", 0, 1, CHECK_PER);
  PixMoguBall   = LoadPixmapSplit("mogu_ball", 0, 1, CHECK_PER);
  PixPata       = LoadPixmapSplit("pata", 0, 3, CHECK_PER);
  PixPataBall   = LoadPixmapSplit("pata_ball", 0, 3, CHECK_PER);
  PixEi         = LoadPixmapSplit("ei", 0, 5, CHECK_PER);
  PixEiBall     = LoadPixmapSplit("ei_ball", 0, 5, CHECK_PER);
  PixKura       = LoadPixmapSplit("kura", 0, 9, CHECK_PER);
  PixManboo     = LoadPixmapSplit("manboo", 0, 5, CHECK_PER);
  PixEnemyShot1 = LoadPixmapSplit("enemyshot1", 0, 1, 0.2);
  PixEnemyShot2 = LoadPixmapSplit("enemyshot2", 0, 6, 0.2);
  PixMissile    = LoadPixmapSplit("missile", 0, 12, 0.3);
  PixEBomb      = LoadPixmaps("ebomb", 0, 3, CHECK_PER);
  PixWarning    = LoadPixmapSplit("warning", 0, 2, CHECK_PER);
  hp_image      = KXL_LoadBitmap(BMP_PATH "/hp.bmp", 255);
  frame_image   = KXL_LoadBitmap(BMP_PATH "/frame.bmp", 0);
  power_image   = KXL_LoadBitmap(BMP_PATH "/power.bmp", 255);
  star_image    = KXL_LoadBitmap(BMP_PATH "/star.bmp", 255);
  for (i=0; i<20; i++) {
    star[i].x=AREA_LX + (rand()%DRAW_WIDTH);
    star[i].y=AREA_LY + (rand()%DRAW_HEIGHT);
    star[i].add=2+(rand()%4);
  }
}

/**********************
  ピックスマップ削除
 **********************/
void DeletePixmap(void)
{
  UnLoadPixmaps(PixMy, 4);
  UnLoadPixmaps(PixMyShot, 2);
  UnLoadPixmaps(PixMyMissile, 2);
  UnLoadPixmaps(PixMyLaser, 1);
  UnLoadPixmaps(PixMyWide, 1);
  UnLoadPixmaps(PixBall, 3);
  UnLoadPixmaps(PixBomb1, 13);
  UnLoadPixmaps(PixBomb2, 9);
  UnLoadPixmaps(PixTobi, 5);
  UnLoadPixmaps(PixIka, 3);
  UnLoadPixmaps(PixIkaBall, 3);
  UnLoadPixmaps(PixUni, 2);
  UnLoadPixmaps(PixMogu, 1);
  UnLoadPixmaps(PixMoguBall, 1);
  UnLoadPixmaps(PixPata, 3);
  UnLoadPixmaps(PixPataBall, 3);
  UnLoadPixmaps(PixEi, 5);
  UnLoadPixmaps(PixEiBall, 5);
  UnLoadPixmaps(PixManboo, 5);
  UnLoadPixmaps(PixKura, 9);
  UnLoadPixmaps(PixEnemyShot1, 1);
  UnLoadPixmaps(PixEnemyShot2, 6);
  UnLoadPixmaps(PixMissile, 12);
  UnLoadPixmaps(PixEBomb, 3);
  UnLoadPixmaps(PixWarning, 2);
  KXL_DeleteImage(frame_image);
  KXL_DeleteImage(power_image);
  KXL_DeleteImage(star_image);
}

/**********************
  ステージデータ読み込み
 **********************/
void LoadStageData(void)
{
  FILE *fp;
  Uint8 buff[256];
  Uint16 dat, i;
  Uint16 bossmax[] = {3, 6, 4, 4};
  
  /* 敵出現データファイルを読み込む */
  sprintf(buff, DATA_PATH "/stage%d.dat", Root->Stage + 1);
  if ((fp = fopen(buff, "r")) == NULL) {
    fprintf(stderr, "next stage not found\n");
    fprintf(stderr, "see you next version...\n");
    Root->MainFlag = MainOpening;
    KXL_PlaySound(0, KXL_SOUND_STOP_ALL);
    return;
  }
  /* 敵出現データを読み込む */
  Root->StageMax = 0;
  while(fgets(buff, 255, fp)) {
    if (buff[0] == ';' || buff[0] == '\n')
      continue;
    if (!Root->StageMax)
      StageDatas = (StageData **)KXL_Malloc(sizeof(StageData *));
    else
      StageDatas = (StageData **)KXL_Realloc(StageDatas, sizeof(StageData *) * (Root->StageMax + 1));
    StageDatas[Root->StageMax] = (StageData *)KXL_Malloc(sizeof(StageData));
    sscanf(buff,"%d, %d, %d, %d",
           &(StageDatas[Root->StageMax]->Time),
           &(StageDatas[Root->StageMax]->CreateNo),
           &(StageDatas[Root->StageMax]->Max),
           &(StageDatas[Root->StageMax]->Step));
    StageDatas[Root->StageMax]->Flag = False;
    StageDatas[Root->StageMax ++]->StepTime = 0;
  }
  fclose(fp);
  
  /* 背景を読み込む */
  sprintf(buff, "%s/back%d.bmp", BMP_PATH, Root->Stage + 1);
  back_image    = KXL_LoadBitmap(buff, 255);
  /* ボスキャラを読み込む */
  sprintf(buff, "boss%d", Root->Stage + 1);
  PixBoss = LoadPixmapSplit(buff, 0, bossmax[Root->Stage], 0.6);
}

/**********************
  ステージデータ削除
 **********************/
void UnLoadStageData()
{
  Uint16 bossmax[] = {3, 6, 4, 4};

  while (Root->StageMax)
    KXL_Free(StageDatas[-- Root->StageMax]);
  KXL_Free(StageDatas);
  KXL_DeleteImage(back_image);
  UnLoadPixmaps(PixBoss, bossmax[Root->Stage]);
}
