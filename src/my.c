#include "geki3.h"
#include "extern.h"

/****************************
  ショット作成
 ****************************/
void CreateMyShot(Sint16 x, Sint16 y, Uint16 dir, Uint16 no)
{
  Sint16 rc;

  Cchr.Attr   = AttrMShot;
  Cchr.Target = AttrEnemy | AttrEBomb;
  Cchr.Strong = 1;
  GetDirectionAdd(dir, &(Cchr.AddX), &(Cchr.AddY), 28);
  Cchr.X      = x;
  Cchr.Y      = y;
  Cchr.Spr    = PixMyShot;
  if ((rc = CopyMyNew(MoveStandardNoFrame, HitDelete, DrawPixmapFrame)) > 0)
    Root->My[rc]->Chr.FrameNo = no;
}

/****************************
  ワイド作成
 ****************************/
void CreateMyWide(CharacterData *my)
{
  Sint16 rc;

  Cchr.Attr    = AttrMShot;
  Cchr.Target  = AttrEnemy | AttrEBomb;
  Cchr.Strong  = 2;
  GetDirectionAdd(90, &(Cchr.AddX), &(Cchr.AddY), 32);
  Cchr.X       = my->X + 20;
  Cchr.Y       = my->Y - 3;
  Cchr.FrameNo = 0;
  Cchr.Spr     = PixMyWide;
  CopyMyNew(MoveStandardNoFrame, HitNone, DrawPixmapFrame);
}

/****************************
  ミサイル発射
 ****************************/
void CreateMyMissile(Sint16 x, Sint16 y, Uint8 no)
{
  Sint16 rc;

  Cchr.Attr     = AttrMShot;
  Cchr.Target   = AttrEnemy | AttrEBomb;
  Cchr.Score    = 30;
  Cchr.Strong   = 5 + Root->MyMissile - ((no + 1) * 4 - 1);
  Cchr.Spr      = PixMyMissile;
  Cchr.X        = x;
  Cchr.Y        = y;
  Cchr.Cnt5     = 45 + no * 90;
  if ((rc = CopyMyNew(MoveMyMissile, HitDelete, DrawPixmapFrame)) >
      -1)
    Root->My[rc]->Chr.FrameNo = no;
  KXL_PlaySound(SE_GO, KXL_SOUND_PLAY);
}

/****************************
  ミサイル移動
 ****************************/
RcHitEnum MoveMyMissile(CharacterData *my)
{
  /*徐々に加速する*/
  GetDirectionAdd(my->Cnt5, &(my->AddX), &(my->AddY), my->Cnt1 ++);
  KXL_DirectionAdd(my->X, my->CntX, my->AddX);
  KXL_DirectionAdd(my->Y, my->CntY, my->AddY);
  if (my->X < AREA_LX - my->Spr[0]->Image->Width ||
      my->X > AREA_RX ||
      my->Y < AREA_LY - my->Spr[0]->Image->Height ||
      my->Y > AREA_RY)
    return RcHitDel;
  return RcHitNone;
}

/****************************
  レーザー移動
 ****************************/
RcHitEnum MoveMyLaser(CharacterData *my)
{
  my->X += 18;
  my->Y = Root->My[0]->Chr.Y + 10;
  if (my->Y < 0 || my->Y > PICT_HEIGHT || my->X < 0 || my->X > PICT_WIDTH)
    return RcHitDel;
  return RcHitNone;
}

/****************************
  レーザー作成
 ****************************/ 
void CreateMyLaser(Sint16 x)
{
  Sint16 rc;

  Cchr.Attr     = AttrMShot;
  Cchr.Target   = AttrEnemy | AttrEBomb;
  Cchr.Strong   = 1;
  Cchr.X        = x;
  Cchr.Spr      = PixMyLaser;
  CopyMyNew(MoveMyLaser, HitNone, DrawPixmapFrame);
  KXL_PlaySound(SE_LASER, KXL_SOUND_PLAY);
}

/****************************
  自分移動
 ****************************/
RcHitEnum MoveMy(CharacterData *my)
{
  static Uint16 mcnt = 0;
  Sint16 sx, sy, i;
  Uint16 no[]={0,0,1,1,2,2,1,1};

  /** ポーズチェック **/
  if (Root->Key & KPause) {
    if (Root->MainFlag == MainGame) {
      Root->MainFlag = MainPause;
      return RcHitNone;
    }
  }
  /** 右に移動 **/
  if (Root->Key & KRight)
    my->X += my->AddX;
  /** 左に移動 **/
  if (Root->Key & KLeft)
    my->X -= my->AddX;
  /** 上に移動 **/
  if (Root->Key & KUp)
    my->Y -= my->AddY;
  /** 下に移動 **/
  if (Root->Key & KDown)
    my->Y += my->AddY;
  /** 移動範囲チェック **/
  if (my->X < AREA_LX)
    my->X = AREA_LX;
  else if (my->X > AREA_RX - my->Spr[0]->Image->Width)
    my->X = AREA_RX - my->Spr[0]->Image->Width;
  if (my->Y < AREA_LY)
    my->Y = AREA_LY;
  else if (my->Y > AREA_RY - my->Spr[0]->Image->Height)
    my->Y = AREA_RY - my->Spr[0]->Image->Height;

  /** 羽ばたく **/
  my->FrameNo = no[mcnt];
  mcnt ++;
  mcnt &= 7;

  /*無敵チェック*/
  if (my->Cnt1 > 0) {
    if (my->Cnt1 > 30) {
      /** 速い点滅 **/
      if (my->Cnt1 % 2) {
	my->FrameNo = 3;
      }
    } else {
      /** 遅い点滅 **/
      if (my->FrameNo == 0) {
	my->FrameNo = 3;
      }
    }
    /** 無敵終了チェック **/
    if (-- my->Cnt1 == 0)
      Root->My[0]->Chr.Target = AttrEnemy | AttrEShot | AttrItem | AttrEBomb;
  }
  /** 武器発射 **/
  if (Root->Key & KShot) {
    if (my->Cnt3 == 0) {
      if (Root->MyLaserPower == 62) {
	my->Cnt3 = 32;
      } else {
	Root->MyLaserPower = 1;
	if (my->Cnt2 == 0) {
	  sx = my->X + my->Spr[my->FrameNo]->Image->Width / 2;
	  sy = my->Y + my->Spr[my->FrameNo]->Image->Height / 2;
	  switch (Root->MyShot / 3) {
	  case 0:
	    CreateMyShot(sx, sy - PixMyShot[0]->Image->Height / 2,
			 90, 0);
	    break;
	  case 1:
	    for (i = 0; i < 2; i ++)
	      CreateMyShot(sx, sy - PixMyShot[0]->Image->Height / 2 - 10 + i * 20,
			   90, 0);
	    break;
	  case 2:
	    for (i = 0; i < 3; i ++)
	      CreateMyShot(sx, sy - PixMyShot[0]->Image->Height / 2 - 20 + i * 20,
			   90, 0);
	    break;
	  case 3:
	    for (i = 0; i < 3; i ++)
	      CreateMyShot(sx, sy - PixMyShot[0]->Image->Height / 2 - 20 + i * 20,
			   90, 0);
	    CreateMyShot(sx, sy - PixMyShot[0]->Image->Height / 2,
			 270, 1);
	    break;
	  case 4:
	    CreateMyWide(my);
	    break;
	  }
	  my->Cnt2 = 2;
	}
	if (my->Cnt4 == 0) {
	  switch (Root->MyMissile / 3) {
	  case 0:
	    break;
	  case 2:
	    CreateMyMissile(my->X + 28, my->Y, 1);
	  case 1:
	    CreateMyMissile(my->X + 28, my->Y + 53, 0);
	    break;
	  }
	  my->Cnt4 = 16;
	}
      }
    } else {
      if (my->Cnt3) {
	CreateMyLaser(my->X + my->Spr[my->FrameNo]->Image->Width - 24);
	Root->MyLaserPower -= 2;
	if (Root->MyLaserPower <= 1)
	  Root->MyLaserPower = 1;
      }
    }
  } else {
    my->Cnt3 = 0;
    if (Root->MyLaserPower < 62)
      Root->MyLaserPower ++;
  }
  if (my->Cnt2 > 0)
    my->Cnt2 --;
  if (my->Cnt3 > 0)
    my->Cnt3 --;
  if (my->Cnt4 > 0)
    my->Cnt4 --;
  return RcHitNone;
}

/****************************
  自分作成
 ****************************/
void ReCreateMy(void) {
  Root->My[0]->Chr.Attr    = AttrMy;
  Root->My[0]->Chr.Target  = AttrItem;
  Root->My[0]->Chr.Cnt1    = 2000 / FRAME_RATE;
  Root->My[0]->Chr.Spr     = PixMy;
  Root->My[0]->Chr.FrameNo = 0;
  Root->My[0]->Move        = MoveMy;
  Root->My[0]->Draw        = DrawPixmapFrame;
  Root->My[0]->Hit         = HitMy;
}

/****************************
  自分爆発
 ****************************/
RcHitEnum MoveMyBomb(CharacterData *my)
{
  if (++ my->FrameNo != my->FrameMax) {
    return RcHitNone;
  } else {
    my->FrameNo = my->FrameMax - 1;
    if (Root->MyHp) { /*残機あり*/
      ReCreateMy();
    } else { /*残機なし*/
      /*ゲームオーバー*/
      Root->Cnt = 0;
      Root->MainFlag = MainGameOver;
      return RcHitDel;
    }
  }
  return RcHitNone;
}

/****************************
  自分当り
 ****************************/
RcHitEnum HitMy(CharacterData *my, CharacterData *your)
{
  if (your->Attr & AttrItem) {
    KXL_PlaySound(SE_POW, KXL_SOUND_PLAY);
    switch (your->FrameNo) {
    case BALL_SHOT:
      if (Root->MyShot < 14)
	Root->MyShot ++;
      break;
    case BALL_MISSILE:
      if (Root->MyMissile < 7)
	Root->MyMissile ++;
      break;
    case BALL_HP:
      Root->MyHp += 20 + (rand() % 2) * 10;
      if (Root->MyHp > Root->MyHpMax)
	Root->MyHp = Root->MyHpMax;
      break;
    }
    return RcHitNone;
  }
  if (my->Cnt1 == 0) { /*無敵以外は爆発準備*/
    Root->MyHp -= your->Strong;
    if (Root->MyHp <= 0)
      Root->MyHp = 0;
    Root->My[0]->Chr.Attr     = AttrNone;
    Root->My[0]->Chr.Target   = AttrNone;
    Root->My[0]->Chr.Spr      = PixBomb2;
    Root->My[0]->Chr.FrameNo  = 0;
    Root->My[0]->Chr.FrameMax = 9;
    Root->My[0]->Move         = MoveMyBomb;
    if (Root->MyShot > 0)
      Root->MyShot --;
    if (Root->MyMissile > 0)
      Root->MyMissile --;
    KXL_PlaySound(SE_BOMB, KXL_SOUND_PLAY);
  }
  return RcHitNone;
}

/****************************
  自分作成
 ****************************/
void CreateMy(void) {
  Uint16 i;

  for (i = 0; i < MAX_YOUR; i ++)
    Root->Your[i]->Chr.Active = False;
  for (i = 0; i < MAX_MY; i ++)
    Root->My[i]->Chr.Active = False;

  Root->MyNo = 1;
  Root->YourNo = 1;
  if (Root->MainFlag == MainGame) {
    Root->MyHpMax      = 64;
    Root->MyHp         = 64;
    Root->Score        = 0;
    Root->MyShot       = 0;
    Root->MyMissile    = 0;
    Root->MyLaserPower = 1;
  }
  Root->ScrollCnt = 0;
  Root->EnemyCnt  = 0;

  ReCreateMy();
  Root->My[0]->Chr.Active   = True;
  Root->My[0]->Chr.Strong   = 100;
  Root->My[0]->Chr.X        = AREA_LX + 20;
  Root->My[0]->Chr.Y        = AREA_LY + (DRAW_HEIGHT - Root->My[0]->Chr.Spr[0]->Image->Height) / 2;
  Root->My[0]->Chr.AddX     = 8;
  Root->My[0]->Chr.AddY     = 8;
  Root->My[0]->Chr.Cnt2     = 0;
  Root->My[0]->Chr.Cnt4     = 0;
  KXL_PlaySound(Root->Stage, KXL_SOUND_PLAY_LOOP);
}
