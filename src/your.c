#include "geki3.h"
#include "extern.h"

/****************************
  爆発
 ****************************/
RcHitEnum MoveBomb(CharacterData *my)
{
  if (++ my->FrameNo != my->FrameMax)
    return RcHitNone;
  return RcHitDel;
}

/****************************
  HPが無くなったら爆発する
 ****************************/
RcHitEnum HitEnemyToBomb(CharacterData *my, CharacterData *your)
{
  my->Hp -= your->Strong;
  if (my->Hp <= 0) {
    KXL_PlaySound(SE_BOMB, KXL_SOUND_PLAY);
    /*爆発*/
    Cchr.Attr     = AttrNone;
    Cchr.Target   = AttrNone;
    Cchr.X        = (my->X + my->Spr[0]->Image->Width / 2)
      - (PixBomb1[0]->Image->Width / 2);
    Cchr.Y        = (my->Y + my->Spr[0]->Image->Height / 2)
      - (PixBomb1[0]->Image->Height / 2);
    Cchr.FrameMax = my->Etc == 0 ? 13 : 9;
    Cchr.Spr      = my->Etc == 0 ? PixBomb1 : PixBomb2;
    CopyYourNew(MoveBomb, HitNone, DrawPixmapFrame);
    return RcHitBomb;
  }
  return RcHitNone;
}

/****************************
  "ボール"移動
 ****************************/
RcHitEnum MoveBall(CharacterData *my)
{
  KXL_DirectionAdd(my->X, my->CntX, my->AddX);
  KXL_DirectionAdd(my->Y, my->CntY, my->AddY);
  if (my->X < AREA_LX - my->Spr[0]->Image->Width)
    return RcHitDel;
  if (my->Y < AREA_LY)
    my->Y = AREA_LY;
  if (my->Y > AREA_RY - my->Spr[0]->Image->Height)
    my->Y = AREA_RY - my->Spr[0]->Image->Height;
  
  if (my->Cnt2 < 8) {
    my->Cnt2 ++;
  } else {
    my->Cnt1 += 10;
    if (my->Cnt1 == 360) {
      my->Cnt1 = 0;
      my->Cnt2 = 0;
    }
    GetDirectionAdd(my->Cnt1 + 270,
		    &(my->AddX),
		    &(my->AddY),
		    4);
  }
  return RcHitNone;
}

/****************************
  HPが無くなったらボール放出
 ****************************/
RcHitEnum HitEnemyToBall(CharacterData *my, CharacterData *your)
{
  Sint16 rc;
  Uint16 r = rand() % 100;

  my->Hp -= your->Strong;
  if (my->Hp <= 0) {
    KXL_PlaySound(SE_BOMB, KXL_SOUND_PLAY);
    /*爆発*/
    Cchr.Attr     = AttrItem;
    Cchr.Target   = AttrMy;
    Cchr.X        = (my->X + my->Spr[0]->Image->Width / 2) -
      (PixBomb1[0]->Image->Width / 2);
    Cchr.Y        = (my->Y + my->Spr[0]->Image->Height / 2)
      - (PixBomb1[0]->Image->Height / 2);
    Cchr.AddX     = 0;
    Cchr.AddY     = 0;
    Cchr.Spr      = PixBall;
    if ((rc = CopyYourNew(MoveBall, HitDelete, DrawPixmapFrame)) > -1) {
      if (Root->MyHp < Root->MyHpMax / 2)
	Root->Your[rc]->Chr.FrameNo = (r > 40 ? BALL_HP :
				       r > 20  ? BALL_MISSILE :
				       BALL_SHOT);
      else if (Root->MyShot == 14)
	Root->Your[rc]->Chr.FrameNo = (r > 50 ? BALL_MISSILE :
				       BALL_HP);
      else if (Root->MyMissile == 7)
	Root->Your[rc]->Chr.FrameNo = (r > 50 ? BALL_SHOT :
				       BALL_HP);
      else if (Root->MyHp == Root->MyHpMax)
	Root->Your[rc]->Chr.FrameNo = (r > 50 ? BALL_SHOT :
				       BALL_MISSILE);
      else
	Root->Your[rc]->Chr.FrameNo = (r > 45 ? BALL_HP :
				       r > 10  ? BALL_MISSILE :
				       BALL_SHOT);
    }
    return RcHitDel;
  }
  return RcHitNone;
}

/****************************
  普通の弾発射
 ****************************/
void CreateEnemyShot(Sint16 x, Sint16 y, Uint16 direction, Uint16
		     speed, Uint8 sel)
{
  Cchr.Attr     = AttrEShot;
  Cchr.Target   = AttrMy;
  Cchr.Strong   = 5;
  GetDirectionAdd(direction, &(Cchr.AddX), &(Cchr.AddY),
		  Root->StageLoop * 2 + speed);
  Cchr.X        = x;
  Cchr.Y        = y;
  Cchr.Spr      = sel == 0 ? PixEnemyShot1 : PixEnemyShot2;
  Cchr.FrameMax = 6;
  CopyYourNew(sel == 0 ? MoveStandardNoFrame : MoveStandard,
	      HitDelete, DrawPixmapFrame);
}

/****************************
  ミサイル発射
 ****************************/
void CreateMissile(CharacterData *my, Sint16 x, Sint16 y)
{
  Sint16 rc;

  Cchr.Attr     = AttrEShot;
  Cchr.Target   = AttrMy;
  Cchr.Strong   = 10;
  Cchr.Spr      = PixMissile;
  Cchr.X        = x;
  Cchr.Y        = y;
  Cchr.Cnt5     = GetDirection(my, &(Root->My[0]->Chr));
  if ((rc = CopyYourNew(MoveMissile, HitDelete, DrawPixmapFrame)) >
      -1)
    Root->Your[rc]->Chr.FrameNo = 
      GetFrameNo(Root->Your[rc]->Chr.Cnt5);
  KXL_PlaySound(SE_GO, KXL_SOUND_PLAY);
}

/****************************
  ミサイル移動
 ****************************/
RcHitEnum MoveMissile(CharacterData *my)
{
  /*徐々に加速する*/
  GetDirectionAdd(my->Cnt5, &(my->AddX), &(my->AddY), my->Cnt1 ++);
  KXL_DirectionAdd(my->X, my->CntX, my->AddX);
  KXL_DirectionAdd(my->Y, my->CntY, my->AddY);
  if (my->X < AREA_LX - my->Spr[0]->Image->Width ||
      my->X > AREA_RX ||
      my->Y < AREA_LY - my->Spr[0]->Image->Height ||
      my->Y > AREA_RY)
    return RcHitDel;
  return RcHitNone;
}

/****************************
  大きくなる弾発射
 ****************************/
void CreateEnemyBomb(Sint16 x, Sint16 y, Uint16 direction, Uint16 speed)
{
  Cchr.Attr     = AttrEBomb;
  Cchr.Target   = AttrMy;
  Cchr.Strong   = 10;
  Cchr.Score    = 5;
  Cchr.Hp       = 2 + Root->Stage;
  Cchr.X        = x;
  Cchr.Y        = y;
  Cchr.Etc      = 1;
  GetDirectionAdd(direction, &(Cchr.AddX), &(Cchr.AddY), speed);
  Cchr.Spr      = PixEBomb;
  Cchr.FrameMax = 3;
  CopyYourNew(MoveEnemyBomb, HitEnemyToBomb, DrawPixmapFrame);
}

/****************************
  大きくなる弾移動
 ****************************/
RcHitEnum MoveEnemyBomb(CharacterData *my)
{
  KXL_DirectionAdd(my->X, my->CntX, my->AddX);
  KXL_DirectionAdd(my->Y, my->CntY, my->AddY);
  if (my->Y < AREA_LY - my->Spr[0]->Image->Height ||
      my->Y > AREA_RY ||
      my->X < AREA_LX - my->Spr[0]->Image->Width ||
      my->X > AREA_RX)
    return RcHitDel;
  my->FrameNo = (my->Cnt1 < 8 ? 0 :
		 my->Cnt1 < 12 ? 1 :
		 2);
  if (my->Cnt1 < 12)
    my->Cnt1 ++;
  return RcHitNone;
}

/****************************
  "いか"出現
 ****************************/
void CreateCuttleFish(Sint16 x, Sint16 y, Uint16 dir, Uint16 speed, Bool ball)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy | AttrMShot;
  Cchr.Strong   = 10;
  Cchr.Hp       = 2 + Root->Stage;
  Cchr.Score    = 8;
  Cchr.Spr      = ball == False ? PixIka : PixIkaBall;
  Cchr.FrameMax = 3;
  Cchr.Etc      = 0;
  Cchr.X        = x;
  Cchr.Y        = y;
  Cchr.Cnt3     = dir;
  Cchr.Cnt4     = 8;
  CopyYourNew(MoveCuttleFish, 
	      ball == False ? HitEnemyToBomb : HitEnemyToBall,
	      DrawPixmapFrame);
}

/****************************
  "いか"移動
 ****************************/
RcHitEnum MoveCuttleFish(CharacterData *my)
{
  /*移動*/
  GetDirectionAdd(my->Cnt3,
		  &(my->AddX),
		  &(my->AddY),
		  my->Cnt4);
  KXL_DirectionAdd(my->X, my->CntX, my->AddX);
  KXL_DirectionAdd(my->Y, my->CntY, my->AddY);
  if (my->X < AREA_LX - my->Spr[0]->Image->Width)
    return RcHitDel;
  if (my->Y < AREA_LY)
    my->Y = AREA_LY;
  if (my->Y > AREA_RY - my->Spr[0]->Image->Height)
    my->Y = AREA_RY - my->Spr[0]->Image->Height;
  /*アニメーション*/
  if (my->Cnt1 == 8) {
    my->FrameNo = 1;
    my->Cnt4 = 1;
  } else if (my->Cnt1 == 12) {
    my->FrameNo = 2;
    my->Cnt4 = 0;
  } else if (my->Cnt1 == 20) {
    my->FrameNo = 1;
    my->Cnt4 = 1;
  } else if (my->Cnt1 == 24) {
    my->FrameNo = 0;
    my->Cnt4 = 8;
    my->Cnt1 = 0;
  }
  my->Cnt1 ++;
  /*攻撃*/
  if (my->Cnt2 == 0) {
    if (my->Y > AREA_LY + 50 && my->Y < AREA_RY)
      if (rand() % (7 - Root->Stage) == 0)
        CreateEnemyShot(my->X + my->Spr[0]->Image->Width / 2,
                         my->Y + my->Spr[0]->Image->Height / 2,
                         GetDirection(my, &(Root->My[0]->Chr)) + (rand() % 20 - 10),
                         4 + Root->Stage, 0);
    my->Cnt2 = 20 - Root->Stage * 4;
  } else
    my->Cnt2 --;
  return RcHitNone;
}

/****************************
  "トビ"出現
 ****************************/
void CreateFlyingFish(Sint16 y, Uint16 sel)
{
  Sint16 ax, ay;

  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy|AttrMShot;
  Cchr.Strong   = 20;
  Cchr.Hp       = 5 + Root->Stage * 2;
  Cchr.Score    = 15;
  Cchr.Spr      = PixTobi;
  Cchr.FrameMax = 5;
  Cchr.Etc      = 1;
  Cchr.X        = AREA_RX;
  Cchr.Y        = y;
  KXL_GetDirectionAdd(270, &ax, &ay);
  Cchr.AddX     = ax * 4;
  KXL_GetDirectionAdd(315 - sel * 90, &ax, &ay);
  Cchr.AddY     = ay * 12;
  Cchr.Cnt3     = sel;
  CopyYourNew(MoveFlyingFish, HitEnemyToBomb, DrawPixmapFrame);
}

/****************************
  "トビ"移動
 ****************************/
RcHitEnum MoveFlyingFish(CharacterData *my)
{
  Sint16 ax, ay;

  /*移動&アニメーション*/
  KXL_DirectionAdd(my->X, my->CntX, my->AddX);
  KXL_DirectionAdd(my->Y, my->CntY, my->AddY);
  if (my->X < AREA_LX - my->Spr[0]->Image->Width)
    return RcHitDel;
  if (my->Cnt3 == 0) { /*下に移動する*/
    if (my->Cnt1) {
      if (my->Cnt1 > 3) {
	my->FrameNo = 2;
      } else if (my->Cnt1 >1) {
	my->FrameNo = 3;
      } else {
	my->FrameNo = 4;
	my->Cnt3 = 1;
	KXL_GetDirectionAdd(225, &ax, &ay);
	my->AddY = ay * 12;
      }
    } else {
      if (my->Y < AREA_RY - my->Spr[0]->Image->Height) {
	my->FrameNo = 0;
      } else {
	my->Cnt1 = 6;
	my->AddY = 0;
	my->FrameNo = 1;
      }
    }
  } else {             /*上に移動する*/
    if (my->Cnt1) {
      if (my->Cnt1 > 3) {
	my->FrameNo = 2;
      } else if (my->Cnt1 >1) {
	my->FrameNo = 1;
      } else {
	my->FrameNo = 0;
	my->Cnt3 = 0;
	KXL_GetDirectionAdd(315, &ax, &ay);
	my->AddY = ay * 12;
      }
    } else {
      if (my->Y > AREA_LY) {
	my->FrameNo = 4;
      } else {
	my->Cnt1 = 6;
	my->AddY = 0;
	my->FrameNo = 3;
      }
    }
  }
  if (my->Cnt1)
    my->Cnt1 --;
  /*攻撃*/
  if (my->Cnt2 == 0) {
    if (rand() % (8 - Root->Stage) == 0)
      CreateEnemyShot(my->X + my->Spr[0]->Image->Width / 2,
		       my->Y + my->Spr[0]->Image->Height / 2,
		       GetDirection(my, &(Root->My[0]->Chr)) + (rand() % 20 - 10),
		       4 + Root->Stage, 0);
    my->Cnt2 = 20 - Root->Stage * 2;
  } else my->Cnt2 --;
  return RcHitNone;
}

/****************************
  "うに"出現
 ****************************/
void CreateSeeUrchin(void)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy | AttrMShot;
  Cchr.Strong   = 30;
  Cchr.Hp       = 10 + Root->Stage * 2;
  Cchr.Score    = 30;
  Cchr.Spr      = PixUni;
  Cchr.FrameMax = 2;
  Cchr.Etc      = 1;
  GetDirectionAdd(270, &(Cchr.AddX), &(Cchr.AddY), 2);
  Cchr.X        = AREA_RX;
  Cchr.Y        = AREA_RY - PixUni[0]->Image->Height;
  CopyYourNew(MoveSeeUrchin, HitEnemyToBomb, DrawPixmapFrame);
}

/****************************
  "うにうに"移動
 ****************************/
RcHitEnum MoveSeeUrchin(CharacterData *my)
{
  Uint16 i;

  /*移動*/
  KXL_DirectionAdd(my->X, my->CntX, my->AddX);
  KXL_DirectionAdd(my->Y, my->CntY, my->AddY);
  if (my->X < AREA_LX - my->Spr[0]->Image->Width)
    return RcHitDel;
  /*アニメーション*/
  my->FrameNo = my->FrameNo ? 0 : 1;
  /*攻撃*/
  if (my->Cnt2 == 0) {
    if (my->Y > AREA_LY + 50 && my->Y < AREA_RY)
      if (rand() % (8 - Root->Stage) == 0)
	for (i=105; i<270; i+=15)
	  CreateEnemyShot(my->X + 10,
			   my->Y + my->Spr[0]->Image->Height / 2,
			   i,
			   4 + Root->StageLoop, 0);
    my->Cnt2 = 20 - Root->Stage * 3;
  } else
    my->Cnt2 --;
  return RcHitNone;
}

/****************************
  "もぐ"出現
 ****************************/
void CreateSubmarine(Bool ball)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy | AttrMShot;
  Cchr.Strong   = 40;
  Cchr.Hp       = 20 + Root->Stage * 2;
  Cchr.Score    = 50;
  Cchr.Spr      = ball == False ? PixMogu : PixMoguBall;
  Cchr.Etc      = 0;
  GetDirectionAdd(270, &(Cchr.AddX), &(Cchr.AddY), 4);
  Cchr.X        = AREA_RX;
  Cchr.Y        = AREA_LY + (rand() % (DRAW_HEIGHT - PixMogu[0]->Image->Height));
  Cchr.Cnt4     = 0;
  Cchr.Cnt5     = 0;
  CopyYourNew(MoveSubmarine, ball == False ? HitEnemyToBomb :
	      HitEnemyToBall,
	      DrawPixmapFrame);
}

/****************************
  "もぐ"移動
 ****************************/
RcHitEnum MoveSubmarine(CharacterData *my)
{
  Sint16 i, s;

  /*移動*/
  KXL_DirectionAdd(my->X, my->CntX, my->AddX);
  KXL_DirectionAdd(my->Y, my->CntY, my->AddY);
  if (my->X < AREA_LX - my->Spr[0]->Image->Width)
    return RcHitDel;
  if (my->Y < AREA_LY)
    my->Y = AREA_LY;
  if (my->Y > AREA_RY - my->Spr[0]->Image->Height)
    my->Y = AREA_RY - my->Spr[0]->Image->Height;
  if (my->Cnt4 == 0) {
    if (rand() % 20 == 0) {
      KXL_GetDirectionAdd(190 + (rand() % 2) * 160, &(my->Cnt2), &(my->Cnt3));
      my->Cnt4 = 1;
    }
  } else {
    if (rand() % 15 == 0) {
      KXL_GetDirectionAdd(270, &(my->Cnt2), &(my->Cnt3));
      my->Cnt4 = 0;
    }
  }
  my->AddX = my->Cnt2 * 4;
  my->AddY = my->Cnt3 * 4;

  /*攻撃*/
  if (my->Y > AREA_LY + 50)
    if (my->Cnt5 == 0) {
      if (rand() % (10 - Root->Stage) == 0) {
	s = rand() % 20;
	for (i=180 + s; i<=340 + s; i+=20)
	  CreateEnemyBomb(my->X + 10,
			  my->Y + my->Spr[0]->Image->Height / 2,
			  i,
			  10 + Root->StageLoop);
      }
      my->Cnt5 = 20 - Root->Stage * 2;
    } else
      my->Cnt5 --;
  return RcHitNone;
}

/****************************
  "ぱた"出現
 ****************************/
void CreateHeadHammerShark(Sint16 y, Bool ball)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy | AttrMShot;
  Cchr.Strong   = 10;
  Cchr.Hp       = 15 + Root->Stage * 2;
  Cchr.Score    = 70;
  Cchr.Spr      = ball == False ? PixPata : PixPataBall;
  Cchr.FrameMax = 3;
  Cchr.Etc      = 1;
  GetDirectionAdd(270, &(Cchr.AddX), &(Cchr.AddY), 6);
  Cchr.X        = AREA_RX;
  Cchr.Y        = y;
  CopyYourNew(MoveHeadHammerShark, 
	      ball == False ? HitEnemyToBomb : HitEnemyToBall,
	      DrawPixmapFrame);
}

/****************************
  "ぱた"移動
 ****************************/
RcHitEnum MoveHeadHammerShark(CharacterData *my)
{
  /*移動*/
  KXL_DirectionAdd(my->X, my->CntX, my->AddX);
  KXL_DirectionAdd(my->Y, my->CntY, my->AddY);
  if (my->Y < AREA_LY - my->Spr[0]->Image->Height ||
      my->Y > AREA_RY ||
      my->X < AREA_LX - my->Spr[0]->Image->Width)
    return RcHitDel;
  if (my->X < AREA_LX + 250 && my->Cnt1 == 0) {
    Uint16 d = GetDirection(my, &(Root->My[0]->Chr));
    GetDirectionAdd(d, 
		    &(my->AddX),
		    &(my->AddY),
		    16);
    my->Cnt1 = 1;
  }
  if (my->Cnt1 == 1) {
    if (my->X > AREA_LX + DRAW_WIDTH / 2)
      my->X = AREA_LX + DRAW_WIDTH / 2;
  }
  /*アニメーション*/
  my->FrameNo ++;
  if (my->FrameNo == my->FrameMax)
    my->FrameNo = 0;
  /*攻撃*/
  if (my->Cnt2 == 0) {
    if (my->Y > AREA_LY + 50 && my->Y < AREA_RY)
      if (rand() % (8 - Root->Stage) == 0)
        CreateEnemyShot(my->X + my->Spr[0]->Image->Width / 2,
                         my->Y + my->Spr[0]->Image->Height / 2,
                         GetDirection(my, &(Root->My[0]->Chr)) + (rand() % 20 - 10),
                         4 + Root->Stage, 0);
    my->Cnt2 = 20 - Root->Stage * 3;
  } else
    my->Cnt2 --;
  return RcHitNone;
}

/****************************
  "まんぼう"出現
 ****************************/
void CreateOceanSunFish(void)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy | AttrMShot;
  Cchr.Strong   = 20;
  Cchr.Hp       = 30 + Root->Stage;
  Cchr.Score    = 100;
  Cchr.Spr      = PixManboo;
  Cchr.X        = AREA_RX;
  Cchr.Y        = AREA_LY + 100 + rand() % 200;
  Cchr.Etc      = 1;
  Cchr.Cnt4     = 0;
  Cchr.Cnt5     = rand() % 3 + 3;
  Cchr.Cnt6     = Cchr.Y < AREA_LY + 20 ? 0 : 1;
  CopyYourNew(MoveOceanSunFish,
              HitEnemyToBomb,
              DrawPixmapFrame);
}

/****************************
  "まんぼう"移動
 ****************************/
RcHitEnum MoveOceanSunFish(CharacterData *my)
{
  Uint8 no[]={0,0,1,1,2,2,3,3,4,4,3,3,2,2,1,1};
  
  /*移動*/
  if (my->Cnt6 == 0) {
    my->Cnt4 ++;
    if (my->Cnt4 >= 2 * my->Cnt5)
      my->Cnt6 = 1;
  } else {
    my->Cnt4 --;
    if (my->Cnt4 <= -2 * my->Cnt5)
      my->Cnt6 = 0;
  }
  my->X -= my->Cnt5;
  my->Y += my->Cnt4;
  if (my->X < AREA_LX - my->Spr[0]->Image->Width)
    return RcHitDel;
  /*アニメーション*/
  if (++ my->Cnt3 > 15)
    my->Cnt3 = 0;
  my->FrameNo = no[my->Cnt3];
  /*攻撃*/
  if (my->Cnt1 == 0) {
    if (rand() % 2)
      CreateMissile(my, my->X - 8, my->Y + 20);
    my->Cnt1 = 12;
  } else
    my->Cnt1 --;
  return RcHitNone;
}

/****************************
  "えい"出現
 ****************************/
void CreateEi(Bool ball)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy | AttrMShot;
  Cchr.Strong   = 20;
  Cchr.Hp       = 5 + Root->Stage;
  Cchr.Score    = 80;
  Cchr.Spr      = ball == False ? PixEi : PixEiBall;
  Cchr.X        = AREA_RX;
  Cchr.Y        = AREA_LY + 100 + rand() % 200;
  Cchr.Etc      = 1;
  Cchr.Cnt4     = 0;
  Cchr.Cnt5     = rand() % 3 + 4;
  Cchr.Cnt6     = Cchr.Y < AREA_LY + 20 ? 0 : 1;
  CopyYourNew(MoveEi,
              ball == False ? HitEnemyToBomb : HitEnemyToBall,
              DrawPixmapFrame);
}

/****************************
  "えい"移動
 ****************************/
RcHitEnum MoveEi(CharacterData *my)
{
  /*移動*/
  if (my->Cnt6 == 0) {
    my->Cnt4 ++;
    if (my->Cnt4 >= 2 * my->Cnt5)
      my->Cnt6 = 1;
  } else {
    my->Cnt4 --;
    if (my->Cnt4 <= -2 * my->Cnt5)
      my->Cnt6 = 0;
  }
  my->X -= my->Cnt5;
  my->Y += my->Cnt4;
  if (my->X < AREA_LX - my->Spr[0]->Image->Width)
    return RcHitDel;
  my->FrameNo = (my->Cnt4 > my->Cnt5  ? 0 :
		 my->Cnt4 > 0         ? 1 :
		 my->Cnt4 < -my->Cnt5 ? 4 :
		 my->Cnt4 < 0         ? 3 :
		 2);
  /*攻撃*/
  if (my->Cnt1 == 0) {
    if (rand() % 2)
      CreateEnemyShot(my->X + my->Spr[0]->Image->Width / 2,
		      my->Y + my->Spr[0]->Image->Height / 2,
		      GetDirection(my, &(Root->My[0]->Chr)) + (rand() % 20 - 10),
		      4 + Root->Stage, 1);
    my->Cnt1 = 20 - Root->Stage * 2;
  } else
    my->Cnt1 --;
  return RcHitNone;
}

/****************************
  "くらげ"出現
 ****************************/
void CreateJellyFish(Sint16 x)
{
  Cchr.Attr     = AttrEnemy;
  Cchr.Target   = AttrMy | AttrMShot;
  Cchr.Strong   = 20;
  Cchr.Hp       = 5 + Root->Stage;
  Cchr.Score    = 60;
  Cchr.Spr      = PixKura;
  Cchr.FrameMax = 9;
  Cchr.X        = x;
  Cchr.Y        = AREA_LY - PixKura[0]->Image->Height;
  Cchr.Etc      = 0;
  Cchr.Cnt4     = 0;
  Cchr.Cnt5     = 4;
  Cchr.Cnt6     = 0;
  CopyYourNew(MoveJellyFish,
              HitEnemyToBomb,
              DrawPixmapFrame);
}

/****************************
  "くらげ"移動
 ****************************/
RcHitEnum MoveJellyFish(CharacterData *my)
{
  /*移動*/
  if (my->Cnt6 == 0) {
    my->Cnt4 ++;
    if (my->Cnt4 >= 2 * my->Cnt5)
      my->Cnt6 = 1;
  } else {
    my->Cnt4 --;
    if (my->Cnt4 <= -2 * my->Cnt5)
      my->Cnt6 = 0;
  }
  my->X += my->Cnt4;
  my->Y += 2;
  if (my->Y > AREA_RY)
    return RcHitDel;
  /*アニメーション*/
  if (++ my->FrameNo == my->FrameMax)
    my->FrameNo = 0;
  /*攻撃*/
  if (my->Cnt1 == 0) {
    if (rand() % 4 == 0)
      CreateEnemyShot(my->X + my->Spr[0]->Image->Width / 2,
		      my->Y + my->Spr[0]->Image->Height / 2,
		      GetDirection(my, &(Root->My[0]->Chr)) + (rand() % 20 - 10),
		      4 + Root->Stage, 1);
    my->Cnt1 = 20 - Root->Stage * 2;
  } else
    my->Cnt1 --;
  return RcHitNone;
}

