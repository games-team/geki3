#ifndef _YOUR_H_
#define _YOUR_H_

/** Ball **/
void CreateItem(Sint16 x, Sint16 y, PixData **pix);
RcHitEnum HitEnemyToBall(CharacterData *my, CharacterData *your);

/** Bomb **/
RcHitEnum MoveBomb(CharacterData *my);
RcHitEnum HitEnemyToBomb(CharacterData *my, CharacterData *your);
RcHitEnum MoveEnemyBomb(CharacterData *my);
RcHitEnum MoveMissile(CharacterData *my);
void CreateEnemyShot(Sint16 x, Sint16 y, Uint16 direction, Uint16
		     speed, Uint8 sel);

/** cuttlesifh **/
void CreateCuttleFish(Sint16 x, Sint16 y, Uint16 dir, Uint16 speed, Bool ball);
RcHitEnum MoveCuttleFish(CharacterData *my);

/** flyingfish **/
void CreateFlyingFish(Sint16 y, Uint16 sel);
RcHitEnum MoveFlyingFish(CharacterData *my);

/** see urchin **/
void CreateSeeUrchin(void);
RcHitEnum MoveSeeUrchin(CharacterData *my);

/** submarine **/
void CreateSubmarine(Bool ball);
RcHitEnum MoveSubmarine(CharacterData *my);

/** head hammer shark **/
void CreateHeadHammerShark(Sint16 y, Bool ball);
RcHitEnum MoveHeadHammerShark(CharacterData *my);

/** ocean sunfish **/
void CreateOceanSunFish(void);
RcHitEnum MoveOceanSunFish(CharacterData *my);

/** ei **/
void CreateEi(Bool ball);
RcHitEnum MoveEi(CharacterData *my);

/** jelly sifh **/
void CreateJellyFish(Sint16 x);
RcHitEnum MoveJellyFish(CharacterData *my);

#endif
